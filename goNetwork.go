package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"regexp"
	"runtime"
	"strings"
	"time"
)

func main() {

	if len(os.Args) < 2 {
		er(fmt.Errorf("no arguments given"))
	}

	t := time.Now()

	var user string
	var pass string
	var cmds string
	var script string
	var mgmt_addr string
	var expect string
	var scriptFile string
	var timeout string = "10"
	var sleep string = "0.2"
	var verbose string = "0"
	var prompt string = "#|\\?|>|\\$"
	var test bool
	var tmp string = "/tmp"
	var log string = fmt.Sprintf("%s_goNetwork.log", t.Format("2006-01-02_15.04.05"))
	var pid string
	var err error

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" {
			fmt.Print("--user=username\tUsername\n")
			fmt.Print("--pass=password\tPassword, leave blank (i.e. --pass= ) for interactive password input\n")
			fmt.Print("--verbose\tTurn on verbose output in log file, default is disabled.\n")
			fmt.Printf("--timeout=INT\tTimeout integer in seconds, default is %s if ommited.\n", timeout)
			fmt.Printf("--sleep=INT\tTime to sleep to wait for each command to complete, default is %s if ommited.\n", sleep)
			fmt.Print("--cmds=filename\tFile of commands to send to remote device (required)\n")
			fmt.Printf("--prompt=REGEX\tRegex patteren to match for expect prompt to match, default is prompt=%s\n", prompt)
			fmt.Print("W.X.Y.Z\t\tManagement Address to connect to\n")
			fmt.Print("--test\t\tThis will display the expect script without executing\n")
			fmt.Print("--help\t\tPrint this help message\n")
			return
		}

		if arg == "--test" {
			test = true
		}

		if arg == "--verbose" {
			verbose = "1"
		}

		if arg == "--pass=" {
			fmt.Printf("Password: ")
			fmt.Scanln(&pass)
			// Clear password from terminal after entry
			fmt.Print("\033[1F\033[10C                                                  \n")
		}

		regex := regexp.MustCompile(`--user=(\S+)`)
		if regex.MatchString(arg) {
			user = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--pass=(\S+)`)
		if regex.MatchString(arg) {
			pass = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--timeout=(-?\d+)`)
		if regex.MatchString(arg) {
			timeout = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--sleep=(\d+\.?\d*)`)
		if regex.MatchString(arg) {
			sleep = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`^\d+\.\d+\.\d+\.\d+$`)
		if regex.MatchString(arg) {
			mgmt_addr = arg
		}

		regex = regexp.MustCompile(`-=prompt=(\S+)`)
		if regex.MatchString(arg) {
			prompt = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--cmds=(\S+)`)
		if regex.MatchString(arg) {
			cmds = regex.FindStringSubmatch(arg)[1]
		}

	}

	if user == "" {
		er(fmt.Errorf("username is not set, use: --user="))
	}

	if pass == "" {
		er(fmt.Errorf("password is not set, use: --pass="))
	}

	if mgmt_addr == "" {
		er(fmt.Errorf("management IP address not set, use: ww.xx.yy.zz"))
	}

	if cmds == "" {
		er(fmt.Errorf("command file not set, use: --cmds="))
	}

	// Check that expect is installed and is in PATH
	expect, err = exec.LookPath("expect")
	er(err)

	// Varible to hold expect script that will be sent to the remote device
	script = "exp_internal " + verbose + "\n" +
		"set timeout " + timeout + "\n" +
		"set username " + user + "\n" +
		"set password " + pass + "\n" +
		"set host " + mgmt_addr + "\n" +
		"log_file " + log + "\n" +
		"expect_before {\n" +
		"  timeout {\n" +
		"    send_user \"connection timeout to $host\\n\"\n" +
		"    exit 0\n" +
		"  }\n" +
		"}\n" +
		"spawn ssh -q -oKexAlgorithms=+diffie-hellman-group1-sha1 -oStrictHostKeyChecking=no $username@$host\n" +
		"expect -re {[Pp][Aa][Ss][Ss][Ww][oO][Rr][Dd].*}\n" +
		"sleep 3\n" +
		"send $password\\n\n" +
		"sleep " + sleep + "\n"

	cmd_file, err := os.Open(cmds)
	er(err)
	defer cmd_file.Close()

	scanner := bufio.NewScanner(cmd_file)

	for scanner.Scan() {
		script = script +
			"expect -re {" + prompt + "}\n" +
			"sleep " + sleep + "\n" +
			"send \"" + strings.ReplaceAll(scanner.Text(), "\"", "\\\"") + "\\n\"\n"
	}

	script += "\n"

	if test {
		fmt.Printf("#Expect Script:\n%s\n\n", script)
		return
	}

	pid = fmt.Sprintf("%d", os.Getpid())
	scriptFile = tmp + "/" + pid + ".tmp"
	fh, err := os.Create(scriptFile)
	er(err)
	_, err = fh.WriteString(script)
	er(err)

	cmd := exec.Command(expect, "-f", scriptFile)
	err = cmd.Run()
	er(err, scriptFile)

	fh.Close()
	err = os.Remove(scriptFile)
	er(err)

}

func er(err error, cleanup ...string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		if len(cleanup) > 0 {
			if cleanup[0] != "" {
				_, err := os.Stat(cleanup[0])
				er(err, "")
				err = os.Remove(cleanup[0])
				er(err, "")
			}
		}
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
